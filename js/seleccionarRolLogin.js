//Detectamos los cambios y mostramos uno u otro form
$('#rol').change(function () {

    if ($('#rol').val() == 1) {
        //formulario de Aspirante
        $("#com1").css("display", "none");
        //formulaario de empresa
        $("#com").css("display", "block");
    };

    if ($('#rol').val() == 2) {
        //formulario de Aspirante
        $("#com1").css("display", "block");
        //formulaario de empresa
        $("#com").css("display", "none");
    };

});
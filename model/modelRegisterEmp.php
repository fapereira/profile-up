<?php
class modelRegisterEmp {

    private $Id_registro_Emp = "";
    private $Email_Emp = "";
    private $Razon_social_Emp = "";
    private $Contrasenia_Emp = "";
}

function getId_registro_Emp() {
    return $this->Id_registro_Emp;
}

 function getEmail_Emp() {
    return $this->Email_Emp;
}

 function getRazon_social_Emp() {
    return $this->Razon_social_Emp;
}

 function getContrasenia_Emp() {
    return $this->Contrasenia_Emp;
}


 function setId_registro_Emp($Id_registro_Emp) {
    $this->Id_registro_Emp = $Id_registro_Emp;
}

 function setEmail_Emp($Email_Emp) {
    $this->Email_Emp = $Email_Emp;
}

 function setRazon_social_Emp($Razon_social_Emp) {
    $this->Razon_social_Emp = $Razon_social_Emp;
}

 function setContrasenia_Emp($Contrasenia_Emp) {
    $this->Contrasenia_Emp = $Contrasenia_Emp;
}

class modelRegisterEmpFunciones {

    public function crearUsuario($Id_registro_Emp, $Email_Emp, $Razon_social_Emp, $Contrasenia_Emp) {
        $cnn = Conexion::getConexion();
        $mensaje = "Informaciòn cargada";
        try {
            $query = $cnn->prepare("INSERT INTO registroemp (Id_registro_Emp, Email_Emp, Razon_social_Emp, Contrasenia_Emp) values(?,?,?,?)");
            $query->bindParam(1, $Id_registro_Emp);
            $query->bindParam(2, $Email_Emp);
            $query->bindParam(3, $Razon_social_Emp);
            $query->bindParam(4, $Contrasenia_Emp);
            $query->execute();

            $mensaje = "Registrado";
        } catch(Exception $e){
            die($e->getMessage());
        }
        $cnn = null;
        return $mensaje;
    }

    public function listarUsuario() {
        $cnn = Conexion::getConexion();
        $mensaje = "";
        try {
            $query = $cnn->prepare("SELECT  * FROM registroemp");
            $query->execute();
            return $query->fetchAll();
        } catch (Exception $ex) {
            $mensaje = $ex->getTraceAsString();
        }
        $cnn = null;
        return $mensaje;
    }

    public function seleccionarUsuario($Razon_social_Emp){
        $cnn = Conexion::getConexion();
        $mensaje = "";
        try {
        $query = $cnn->prepare("SELECT * FROM registroemp WHERE Razon_social_Emp = ?");
        $query ->bindParam(1,$Razon_social_Emp);
        $query->execute();
        return $query->fetch();
        } catch (Exception $ex) {
            $mensaje = $ex->getTraceAsString();
        }
          $cnn = null;
        return $mensaje;
    }
    
    public function editarUsuario($Id_registro_Emp){
        $cnn = Conexion::getConexion();
        $mensaje = "";
        try {
          $query = $cnn ->prepare("UPDATE registroemp SET Email_Emp = ?, Razon_social_Emp = ?, Contrasenia_Emp = ?,
                                                    WHERE Id_registro_Emp = ?");
        $query ->bindParam(1, $Id_registro);
        $query ->execute();
        
        $mensaje="Actualizado";  
        } catch (Exception $ex) {
            $mensaje = $ex->getTraceAsString();
        }
        $cnn = null;
        return $mensaje;
    }
    
    
    public function borrarUsuario($Id_registro){
        $cnn = Conexion::getConexion();
        $mensaje = "";
        try {
        $query = $cnn->prepare("DELETE * FROM usuario WHERE  Id_registro= ?");
        $query ->bindParam(1, $Id_registro);
        $query ->execute();
        
        $mensaje = "Eliminado";
        } catch (Exception $ex) {
            $mensaje = $ex->getTraceAsString();
        }
        $cnn = null;
        return $mensaje;
    }

    
    public function listaRoles($rol){
        $cnn = Conexion::getConexion();
        $mensaje = "";
        $query = $cnn->prepare("SELECT documentoUsuario, passwordUsuario, Rol.DescripcionRol  FROM usuario 
                                                     INNER JOIN Rol ON usuario.documentoUsuario = Rol.documentoUsuario");
        $query-> bindParameter(1,$rol);
        $query->execute();
        return $query->fetchAll();        
    }
    
    public function iniciarSesion($Razon_social_Emp, $Contrasenia_Emp)
    {
        
        $cnn = Conexion::getConexion();
        $mensaje = "";
        try {
        $query = $cnn->prepare("SELECT * FROM registroemp WHERE Razon_social_Emp = ? AND Contrasenia_Emp = ?");    
        $query->bindParam(1,$Razon_social_Emp);
        $query->bindParam(2,$Contrasenia_Emp);
        $query->execute();
        $mensaje = "Ingreso Exitoso";
        return $query->fetchObject();
        } catch (Exception $ex) {
            $mensaje = $ex->getTraceAsString();
        }
        $cnn = null;
        return $mensaje;
    }    
}

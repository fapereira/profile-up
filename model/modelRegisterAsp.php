<?php
class modelRegisterAsp {

    private $Id_registro = "";
    private $Email_Asp = "";
    private $Usuario_Asp = "";
    private $Contrasenia_Asp = "";
}

function getId_registro() {
    return $this->Id_registro;
}

 function getEmail_Asp() {
    return $this->Email_Asp;
}

 function getUsuario_Asp() {
    return $this->Usuario_Asp;
}

 function getContrasenia_Asp() {
    return $this->Contrasenia_Asp;
}


 function setId_registro($Id_registro) {
    $this->Id_registro = $Id_registro;
}

 function setEmail_Asp($Email_Asp) {
    $this->Email_Asp = $Email_Asp;
}

 function setUsuario_Asp($Usuario_Asp) {
    $this->Usuario_Asp = $Usuario_Asp;
}

 function setContrasenia_Asp($Contrasenia_Asp) {
    $this->Contrasenia_Asp = $Contrasenia_Asp;
}

class modelRegisterAspFunciones {

    public function crearUsuario($Id_registro, $Email_Asp, $Usuario_Asp, $Contrasenia_Asp) {
        $cnn = Conexion::getConexion();
        $mensaje = "Informaciòn cargada";
        try {
            $query = $cnn->prepare("INSERT INTO registro (Id_registro, Email_Asp, Usuario_Asp, Contrasenia_Asp) values(?,?,?,?)");
            $query->bindParam(1, $Id_registro);
            $query->bindParam(2, $Email_Asp);
            $query->bindParam(3, $Usuario_Asp);
            $query->bindParam(4, $Contrasenia_Asp);
            $query->execute();

            $mensaje = "Registrado";
        } catch(Exception $e){
            die($e->getMessage());
        }
        $cnn = null;
        return $mensaje;
    }

    public function listarUsuario() {
        $cnn = Conexion::getConexion();
        $mensaje = "";
        try {
            $query = $cnn->prepare("SELECT  * FROM registro");
            $query->execute();
            return $query->fetchAll();
        } catch (Exception $ex) {
            $mensaje = $ex->getTraceAsString();
        }
        $cnn = null;
        return $mensaje;
    }

    public function seleccionarUsuario($Usuario_Asp){
        $cnn = Conexion::getConexion();
        $mensaje = "";
        try {
        $query = $cnn->prepare("SELECT * FROM registro WHERE Usuario_Asp = ?");
        $query ->bindParam(1,$Usuario_Asp);
        $query->execute();
        return $query->fetch();
        } catch (Exception $ex) {
            $mensaje = $ex->getTraceAsString();
        }
          $cnn = null;
        return $mensaje;
    }
    
    public function editarUsuario($Id_registro){
        $cnn = Conexion::getConexion();
        $mensaje = "";
        try {
          $query = $cnn ->prepare("UPDATE registro SET Email_Asp = ?, Usuario_Asp = ?, Contrasenia_Asp = ?,
                                                    WHERE Id_registro = ?");
        $query ->bindParam(1, $Id_registro);
        $query ->execute();
        
        $mensaje="Actualizado";  
        } catch (Exception $ex) {
            $mensaje = $ex->getTraceAsString();
        }
        $cnn = null;
        return $mensaje;
    }
    
    
    public function borrarUsuario($Id_registro){
        $cnn = Conexion::getConexion();
        $mensaje = "";
        try {
        $query = $cnn->prepare("DELETE * FROM registro WHERE  Id_registro= ?");
        $query ->bindParam(1, $Id_registro);
        $query ->execute();
        
        $mensaje = "Eliminado";
        } catch (Exception $ex) {
            $mensaje = $ex->getTraceAsString();
        }
        $cnn = null;
        return $mensaje;
    }

    
    public function listaRoles($rol){
        $cnn = Conexion::getConexion();
        $mensaje = "";
        $query = $cnn->prepare("SELECT documentoUsuario, passwordUsuario, Rol.DescripcionRol  FROM usuario 
                                                     INNER JOIN Rol ON usuario.documentoUsuario = Rol.documentoUsuario");
        $query-> bindParameter(1,$rol);
        $query->execute();
        return $query->fetchAll();        
    }
    
    public function iniciarSesion($Usuario_Asp, $Contrasenia_Asp)
    {
        
        $cnn = Conexion::getConexion();
        $mensaje = "";
        try {
        $query = $cnn->prepare("SELECT * FROM registro WHERE Usuario_Asp = ? AND Contrasenia_Asp = ?");    
        $query->bindParam(1,$Usuario_Asp);
        $query->bindParam(2,$Contrasenia_Asp);
        $query->execute();
        $mensaje = "Ingreso Exitoso";
        return $query->fetchObject();
        } catch (Exception $ex) {
            $mensaje = $ex->getTraceAsString();
        }
        $cnn = null;
        return $mensaje;
    }     
}

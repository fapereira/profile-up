<?php
class modelUsuario {

    private $documentoUsuario = "";
    private $idCiudad = 0;
    private $apellido1Usuario = "";
    private $apellido2Usuario = "";
    private $nombre1Usuario = "";
    private $nombre2Usuario = "";
    private $passwordUsuario = "";
    private $fotoUsuario = "";
    private $correoUsuario = "";
    private $telefonoUsuario = "";
    private $celularUsuario = "";
    private $generoUsuario = "";
    private $fechaNacimientoUsuario = "";
    private $tipoDocumentoUsuario = "";
    private $estadoCivilUsuario = "";
    private $estadoUsuario = "";
    
}

function getDocumentoUsuario() {
    return $this->documentoUsuario;
}

 function getIdCiudad() {
    return $this->idCiudad;
}

 function getApellido1Usuario() {
    return $this->apellido1Usuario;
}

 function getApellido2Usuario() {
    return $this->apellido2Usuario;
}

 function getNombre1Usuario() {
    return $this->nombre1Usuario;
}

 function getNombre2Usuario() {
    return $this->nombre2Usuario;
}

 function getPasswordUsuario() {
    return $this->passwordUsuario;
}

 function getFotoUsuario() {
    return $this->fotoUsuario;
}

 function getCorreoUsuario() {
    return $this->correoUsuario;
}

 function getTelefonoUsuario() {
    return $this->telefonoUsuario;
}

 function getCelularUsuario() {
    return $this->celularUsuario;
}

 function getGeneroUsuario() {
    return $this->generoUsuario;
}

 function getFechaNacimientoUsuario() {
    return $this->fechaNacimientoUsuario;
}

 function getTipoDocumentoUsuario() {
    return $this->tipoDocumentoUsuario;
}

 function getEstadoCivilUsuario() {
    return $this->estadoCivilUsuario;
}

 function getEstadoUsuario() {
    return $this->estadoUsuario;
}

 function setDocumentoUsuario($documentoUsuario) {
    $this->documentoUsuario = $documentoUsuario;
}

 function setIdCiudad($idCiudad) {
    $this->idCiudad = $idCiudad;
}

 function setApellido1Usuario($apellido1Usuario) {
    $this->apellido1Usuario = $apellido1Usuario;
}

 function setApellido2Usuario($apellido2Usuario) {
    $this->apellido2Usuario = $apellido2Usuario;
}

 function setNombre1Usuario($nombre1Usuario) {
    $this->nombre1Usuario = $nombre1Usuario;
}

 function setNombre2Usuario($nombre2Usuario) {
    $this->nombre2Usuario = $nombre2Usuario;
}

 function setPasswordUsuario($passwordUsuario) {
    $this->passwordUsuario = $passwordUsuario;
}

 function setFotoUsuario($fotoUsuario) {
    $this->fotoUsuario = $fotoUsuario;
}

 function setCorreoUsuario($correoUsuario) {
    $this->correoUsuario = $correoUsuario;
}

 function setTelefonoUsuario($telefonoUsuario) {
    $this->telefonoUsuario = $telefonoUsuario;
}

 function setCelularUsuario($celularUsuario) {
    $this->celularUsuario = $celularUsuario;
}

 function setGeneroUsuario($generoUsuario) {
    $this->generoUsuario = $generoUsuario;
}

 function setFechaNacimientoUsuario($fechaNacimientoUsuario) {
    $this->fechaNacimientoUsuario = $fechaNacimientoUsuario;
}

 function setTipoDocumentoUsuario($tipoDocumentoUsuario) {
    $this->tipoDocumentoUsuario = $tipoDocumentoUsuario;
}

 function setEstadoCivilUsuario($estadoCivilUsuario) {
    $this->estadoCivilUsuario = $estadoCivilUsuario;
}

 function setEstadoUsuario($estadoUsuario) {
    $this->estadoUsuario = $estadoUsuario;
}

class modelUsuarioFunciones {

    public function crearUsuario(usuario $usuario) {
        $cnn = Conexion::getConexion();
        $mensaje = "";
        try {
            $query = $cnn->prepare("INSERT INTO usuario values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $query->bindParam(1, $usuario->getDocumentoUsuario());
            $query->bindParam(2, $usuario->getidCiudad());
            $query->bindParam(3, $usuario->getApellido1Usuario());
            $query->bindParam(4, $usuario->getApellido2Usuario());
            $query->bindParam(5, $usuario->getNombre1Usuario());
            $query->bindParam(6, $usuario->getNombre2Usuario());
            $query->bindParam(7, $usuario->getPasswordUsuario());
            $query->bindParam(8, $usuario->getFotoUsuario());
            $query->bindParam(9, $usuario->getCorreoUsuario());
            $query->bindParam(10, $usuario->getTelefonoUsuario());
            $query->bindParam(11, $usuario->getCelularUsuario());
            $query->bindParam(12, $usuario->getGeneroUsuario());
            $query->bindParam(13, $usuario->getFechaNacimientoUsuario());
            $query->bindParam(14, $usuario->getTipoDocumentoUsuario());
            $query->bindParam(15, $usuario->getEstadoCivilUsuario());
            $query->bindParam(16, $usuario->getEstadoUsuario());
            $query->execute();

            $mensaje = "Registrado";
        } catch (Exception $ex) {
            $mensaje = $ex->getTraceAsString();
        }
        $cnn = null;
        return $mensaje;
    }

    public function listarUsuario() {
        $cnn = Conexion::getConexion();
        $mensaje = "";
        try {
            $query = $cnn->prepare("SELECT  * FROM usuario");
            $query->execute();
            return $query->fetchAll();
        } catch (Exception $ex) {
            $mensaje = $ex->getTraceAsString();
        }
        $cnn = null;
        return $mensaje;
    }

    public function seleccionarUsuario($nombres){
        $cnn = Conexion::getConexion();
        $mensaje = "";
        try {
        $query = $cnn->prepare("SELECT * FROM usuario WHERE nombre1Usuario = ?");
        $query ->bindParam(1,$nombres);
        $query->execute();
        return $query->fetch();
        } catch (Exception $ex) {
            $mensaje = $ex->getTraceAsString();
        }
          $cnn = null;
        return $mensaje;
    }
    
    public function editarUsuario($documento){
        $cnn = Conexion::getConexion();
        $mensaje = "";
        try {
          $query = $cnn ->prepare("UPDATE usuario SET idCiudad = ?, apellido1Usuario = ?, apellido2Usuario = ?,
                                                                                             nombre1Usuario = ?, nombre2Usuario = ?, passwordUsuario = ?, 
                                                                                             fotoUsuario = ?, correoUsuario = ?, telefonoUsuario = ?,
                                                                                             celularUsuario = ?, generoUsuario = ?, fechaNacimientoUsuario = ?,
                                                                                             tipoDocumentoUsuario = ?, estadoCivilUsuario = ?, estadoUsuario = ?
                                                    WHERE documentoUsuario = ?");
        $query ->bindParam(1, $documento);
        $query ->execute();
        
        $mensaje="Actualizado";  
        } catch (Exception $ex) {
            $mensaje = $ex->getTraceAsString();
        }
        $cnn = null;
        return $mensaje;
    }
    
    
    public function borrarUsuario($documento){
        $cnn = Conexion::getConexion();
        $mensaje = "";
        try {
        $query = $cnn->prepare("DELETE * FROM usuario WHERE  documentoUsuario= ?");
        $query ->bindParam(1, $documento);
        $query ->execute();
        
        $mensaje = "Eliminado";
        } catch (Exception $ex) {
            $mensaje = $ex->getTraceAsString();
        }
        $cnn = null;
        return $mensaje;
    }

    
    public function listaRoles($rol){
        $cnn = Conexion::getConexion();
        $mensaje = "";
        $query = $cnn->prepare("SELECT documentoUsuario, passwordUsuario, Rol.DescripcionRol  FROM usuario 
                                                     INNER JOIN Rol ON usuario.documentoUsuario = Rol.documentoUsuario");
        $query-> bindParameter(1,$rol);
        $query->execute();
        return $query->fetchAll();        
    }
    
    public function iniciarSesion($documento, $clave){
        
        $cnn = Conexion::getConexion();
        $mensaje = "";
        try {
        $query = $cnn->prepare("SELECT * FROM registro | registroemp WHERE Usuario_Asp = ? | Razon_social_Emp AND Contrasenia_Asp = ? | Contrasenia_Emp");    
        $query->bindParam(1,$documento);
        $query->bindParam(2, $clave);
        $query->execute();
        $mensaje = "Ingreso Exitoso";
        return $query->fetchObject();
        } catch (Exception $ex) {
            $mensaje = $ex->getTraceAsString();
        }
        $cnn = null;
        return $mensaje;
    }    
}
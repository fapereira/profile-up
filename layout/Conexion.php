<?php 
$conexion=Conexion::getConexion();
class conexion {
	public static function getConexion(){
		$conn = null;
		try{
			$conn = new PDO("mysql:host=127.0.0.1;dbname=profileup", "root", "");
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}catch (PDOException $ex){
			echo 'Error: '.$ex->getMessage();
		}
		return $conn;
	}
}
?>
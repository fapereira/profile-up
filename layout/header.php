<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
</head>

<body>
   <header id="header">
        <nav class="navbar navbar-fixed-top" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <figure>
                        <a href="../index.php"><img class="icon-profile" src="../fonts/loGo_3.svg" ></a>
                    </figure>
                </div>
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="../index.php">Home</a></li>
                        <li><a href="../views/about-us.php">Sobre nosotros</a></li>
                        <li><a href="../views/services.php">Servicios</a></li>
                        <li><a href="../views/portfolio.php">Portafolio</a></li>
                        <li><a href="../views/blog.php">Blog</a></li>
                        <li><a href="../views/contact-us.php">Contacto</a></li>      
                        <li class="dropdown"><a class="dropdown-toggle" href="../index.php" data-toggle="dropdown" role="button" aria-haspopup="true"><i class="fa fa-user fa-lg"></i></a>
                            <ul class="dropdown-menu settings-menu">
                                <li><a href="../views/register.php"><i class="fa fa-user fa-lg"></i> Registrarse</a></li>
                                <li><a href="../views/login.php"><i class="fa fa-sign-out fa-lg"></i> Iniciar sesión</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
    </header><!--/header-->
</body>
</html>
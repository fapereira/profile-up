<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
</head>

<body>
  <section id="bottom">
        <div class="container wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="widget">
                        <h3>EMPRESAS</h3>
                        <ul>
                            <li><a href="#">Sobre nosotros</a></li>
                            <li><a href="#">Conocer al equipo</a></li>
                            <li><a href="#">Derechos de autor</a></li>                           
                        </ul>
                    </div>    
                </div>

                <div class="col-md-4 col-sm-4">
                    <div class="widget">
                        <h3>APOYO</h3>
                        <ul>
                            <li><a href="#">Preguntas más frecuentes</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Documentación</a></li>                          
                        </ul>
                    </div>    
                </div>

                <div class="col-md-4 col-sm-4">
                    <div class="widget">
                        <h3>ASPIRANTES</h3>
                        <ul>
                            <li><a href="#">Desarrollo web</a></li>
                            <li><a href="#">Tema</a></li>
                            <li><a href="#">Desarrollo</a></li>
                        </ul>
                    </div>    
                </div>
            </div>
        </div>
    </section>
   <div class="top-bar">
		<div class="container">
			<div class="row">
			    <div class="col-lg-12">
				   <div class="social">
						<ul class="social-share">
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin"></i></a></li> 
							<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
							<li><a href="#"><i class="fa fa-skype"></i></a></li>
						</ul>
				   </div>
                </div>
			</div>
		</div>
	</div>
	
	<footer id="footer" class="midnight-blue">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; 2017. Todos los derechos reservados.
                </div>
                
                <div class="col-sm-6">
                    <ul class="pull-right">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Sobre nosotros</a></li>
                        <li><a href="#">Preguntas más frecuentes</a></li>
                        <li><a href="#">Contáctenos</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer><!--/#footer-->
</body>
</html>
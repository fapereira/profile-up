<?php

require '../model/modelRegisterEmp.php';
require '../layout/Conexion.php';

$uSesion = new modelRegisterEmpFunciones();
$userdto = new modelRegisterEmp();
$udao = new modelRegisterEmpFunciones();

session_start();

if (isset($_POST['ingresar'])){
    $Razon_social_Emp = $_POST['Razon_social_Emp'];
    $Contrasenia_Emp = $_POST['Contrasenia_Emp'];
    try {
        $userdto = $uSesion->iniciarSesion($Razon_social_Emp, $Contrasenia_Emp);
    if ($userdto != null) {
            $_SESSION['auth'] = $userdto;
            header("location: ../views/datosUsuarioEmp.php?mensaje=" . $mensaje);
        }else{
            header("location: ../views/login.php?mensaje=" . $mensaje);
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }

}
if (isset($_GET['sesion']) =="false") {
    session_destroy();
    header("Location: ../index.php");
}
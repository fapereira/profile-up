<?php

require '../model/modelRegisterAsp.php';
require '../layout/Conexion.php';

$uSesion = new modelRegisterAspFunciones();
$userdto = new modelRegisterAsp();
$udao = new modelRegisterAspFunciones();

session_start();

if (isset($_POST['ingresar'])){
    $Usuario_Asp = $_POST['Usuario_Asp'];
    $Contrasenia_Asp = $_POST['Contrasenia_Asp'];
    try {
        $userdto = $uSesion->iniciarSesion($Usuario_Asp, $Contrasenia_Asp);
    if ($userdto != null) {
            $_SESSION['auth'] = $userdto;
            header("location: ../views/datosUsuario.php?mensaje=" . $mensaje);
        }else{
            header("location: ../views/login.php?mensaje=" . $mensaje);
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }

}
if (isset($_GET['sesion']) =="false") {
    session_destroy();
    header("Location: ../index.php");
}
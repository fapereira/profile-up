<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php session_start();?>
<html>

<head>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="../css/main1.css" rel="stylesheet">
    <link href="../css/responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/font2-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../css/inicioAspi.css">

    <title></title>
</head>

<body class="container">
    <?php include_once("../layout/header.php");?>
    <br>
    <br>
    <br>
    <h3>Datos Usuarios</h3>
    <a href="../controller/controllerRegisterAsp.php?sesion=false">Cerrar Sesion</a>
    <section class="container-fluid sin_padding_side padding_top_down">
        <main class="container-fluid-offset">
            <div class="col-lg-10 col-xs-12 container_list">
                <div class="bx-table table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Nº</th>
                                <th>Email</th>
                                <th>Usuario</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                require '../model/modelRegisterAsp.php';
                                require '../layout/Conexion.php';
                                $uDAO = new modelRegisterAspFunciones();
                                $allusers = $uDAO->listarUsuario();
                                foreach ($allusers as $registro){
                                ?>
                                <tr>
                                    <td>
                                        <?php echo $registro['Id_registro'];?>
                                    </td>
                                    <td>
                                        <?php echo $registro['Email_Asp'];?>
                                    </td>
                                    <td>
                                        <?php echo $registro['Usuario_Asp'];?>
                                    </td>
                                </tr>
                                <?php

                                }
                                ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </main>
    </section>
    <?php include("../layout/footer.php");?>
    <!-- jQuery necessary for Bootstrap's JavaScript plugins -->
    <script src="../js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/custom.js" type="text/javascript"></script>
</body>

</html>

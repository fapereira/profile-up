<!DOCTYPE html>
<?php session_start();?>
<html class="col-xs-12.fluid">

<head>
    <title>Ingreso - ProfileUp</title>
    <!-- Bootstrap -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../css/font-awesome.min.css">
    <link href="../../css/animate.min.css" rel="stylesheet">
    <link href="../../css/prettyPhoto.css" rel="stylesheet">

    <link href="../../css/main.css" rel="stylesheet">
    <link href="../../css/responsive.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../css/login1.css" media="screen">
    <link rel="stylesheet" type="text/css" href="../../css/font-awesome.min.css">


    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
</head>

<body class="fondo1 col-xs-12-fluid">

    <section id="principal" class="col-xs-12-fluid">

        <!-- section 1 -->

        <div class="contenedor">
            <figure class="logo">
                <a href="../../index.php"><img src="../../fonts/loGo_3.svg" alt="Smart Up Logo" class="../../fonts/loGo_3.svg"></a>
            </figure>

            <p>Sistema de gestión de hojas de vida y pruebas psicotecnicas.<br>Ingresa con tu usuario y contraseña</p>

            <div>
                <!-- Trigger the modal with a button -->
                <button id="bt1" type="button" class=" btn-default btn-sm" data-toggle="modal" data-target="#myModal">Ingreso Aspirantes</button>
                <button id="bt" type="button" class=" btn-default btn-sm" data-toggle="modal" data-target="#myModal1">Ingreso Empresas</button>

                <!-- Modal -->
                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Aspirantes</h4>
                            </div>
                            <div class="modal-body">
                                <!-- Formulario de inicio Para Aspirante -->

                                <form id="com1" class="login_form" action="../../controller/controllerUsuario.php" method="POST">
                                    <div class="icon_form usuario">
                                        <label class="fondo1 cblanco usuario" for="usuario" title="Usuario"></label>
                                        <input id="usuario" type="text" name="Usuario_Asp" class="input" placeholder="Usuario" required="required">
                                    </div>
                                    <div class="icon_form contraseña">
                                        <label class="fondo1 cblanco contrasena" for="contraseña" title="Contraseña"></label>
                                        <input id="contraseña" type="password" autocomplete="off" name="Contrasenia_Asp" class="input" placeholder="Contraseña" required="required">
                                    </div>
                                    <div class="icon_form button">
                                        <input type="submit" class="btn fondo2 cblanco cblancoh fondoh1" name="ingresar" value="Ingresar">
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
                                        <!-- Segundo modal -->
            <div>
                <!-- Modal -->
                <div class="modal fade" id="myModal1" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Empresas</h4>
                            </div>
                            <div class="modal-body">
                                <!-- Formulario de inicio Para Empresas -->

                                <form id="com" class="login_form" action="../../controller/controllerUsuarioEmp.php" method="POST">
                                    <div class="icon_form usuario">
                                        <label class="fondo1 cblanco usuario" for="usuario" title="Usuario"></label>
                                        <input id="usuario" type="text" name="Razon_social_Emp" class="input" placeholder="Razón Social" required="required">
                                    </div>
                                    <div class="icon_form contraseña">
                                        <label class="fondo1 cblanco contrasena" for="contraseña" title="Contraseña"></label>
                                        <input id="contraseña" type="password" autocomplete="off" name="Contrasenia_Emp" class="input" placeholder="Contraseña" required="required">
                                    </div>
                                    <div class="icon_form button">
                                        <input type="submit" class="btn fondo2 cblanco cblancoh fondoh1" name="ingresar" value="Ingresar">
                                    </div>
                                </form>
                            </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>                        
                        </div>
                    </div>

                </div>
            </div>

                <span class="forget"><a class="color1 colorh2" href="recuperarPass.php">¿Olvidó su contraseña?</a></span>
                <span class="forget"><a class="color1 colorh2" href="register.php">¿Aún no esta registrado?</a></span>
                <span class="forget"><a class="color1 colorh2" href="../../index.php">pagina de inicio</a></span>

        </div>
        <!-- fin section 1 -->

    </section>
    <?php include("../../layout/footer.php");?>
    <!--  jQuery Bootstraps JavaScript plugins -->
    <script src="../../js/jquery.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/jquery.prettyPhoto.js"></script>
    <script src="../../js/jquery.isotope.min.js"></script>
    <script src="../../js/wow.min.js"></script>
    <script src="../../js/main.js"></script>
</body>

</html>

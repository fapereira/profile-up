<!DOCTYPE html>
<?php session_start();?>
<html class="col-xs-12.fluid">

<head>
    <title>Ingreso - ProfileUp</title>
    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="../css/prettyPhoto.css" rel="stylesheet">

    <link href="../css/main.css" rel="stylesheet">
    <link href="../css/responsive.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="../css/inicioAspi1.css" media="screen">
    <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

    <script src="../Elementos/assets/js/jquery-2.1.4.min.js" type="text/javascript"></script>
    <script src="../js/bootstrap.min.js" type="text/javascript"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
</head>

<body class="fondo1 col-xs-12-fluid">

    <section id="principal" class="col-xs-12-fluid">

        <!-- section 1 -->

        <div class="contenedor border2">
            <figure class="logo">
                <a href="../index.php"><img src="../fonts/loGo_3.svg" alt="Smart Up Logo" class="../Elementos/fonts/loGo_3.svg"></a>
            </figure>

            <p>Sistema de gestión de hojas de vida y pruebas.<br>Registrate, eligiendo el rol y completando los campos solicitados</p>
            <!--            Fromulario De Selecciòn-->
            <form>
                <div class="icon_form email">
                    <label class="fondo1 cblanco fa-group" for="rol" title="Rol"></label>
                    <select id="rol" class="input" required="required">
                    <option value="0" disabled selected>Selección:</option>
                        //Traemos informacion de la base de datos
                            <?php
                              $mysqli = new mysqli('127.0.0.1', 'root', '', 'profileup');

                              $query = $mysqli -> query ("SELECT * FROM rol");

                              while ($valores = mysqli_fetch_array($query)) {

                                echo '<option value="'.$valores[Id_rol].'">'.$valores[Descripcion_rol].'</option>';

                              }
                            ?>
                  </select>
                    <script src="../js/selecionarRol.js"></script>
                </div>
            </form>
            <!--            Fromulario De Aspirantes-->
            <form class="com1" action="../controller/controllerRegisterAsp.php" method="POST" style="display: none;">
                <div class="hidden">
                    <input type="hidden" name="Id_registro">
                </div>
                <div class="icon_form email">
                    <label class="fondo1 cblanco email" for="email" title="Email"></label>
                    <input id="email" type="email" name="Email_Asp" class="input" placeholder="Email" required="required">
                </div>
                <div class="icon_form usuario">
                    <label class="fondo1 cblanco usuario" for="usuario" title="Usuario"></label>
                    <input id="usuario" type="text" name="Usuario_Asp" class="input" placeholder="Usuario" required="required">
                </div>
                <div class="icon_form contrasena">
                    <label class="fondo1 cblanco contrasena" for="contraseña" title="Contraseña"></label>
                    <input id="contraseña" type="password" autocomplete="off" name="Contrasenia_Asp" class="input" placeholder="Contraseña" required="required">
                </div>
                <div class="icon_form button">
                    <input id="boton" type="submit" class="btn fondo2 cblanco cblancoh fondoh1" name="resgistrar" value="Registrar">
                </div>
            </form>
            <!--            Fromulario De Empresas-->
            <form class="com" action="../controller/controllerRegisterEmp.php" method="POST" style="display: none;">
                <div class="hidden">
                    <input type="hidden" name="Id_registro_Emp">
                </div>
                <div class="icon_form email">
                    <label class="fondo1 cblanco email" for="email" title="Email"></label>
                    <input id="email" type="email" name="Email_Emp" class="input" placeholder="Email empresa" required="required">
                </div>
                <div class="icon_form usuario">
                    <label class="fondo1 cblanco fa-building-o" for="usuario" title="Usuario"></label>
                    <input id="usuario" type="text" name="Razon_social_Emp" class="input" placeholder="Razòn social" required="required">
                </div>
                <div class="icon_form contrasena">
                    <label class="fondo1 cblanco contrasena" for="contraseña" title="Contraseña"></label>
                    <input id="contraseña" type="password" autocomplete="off" name="Contrasenia_Emp" class="input" placeholder="Contraseña" required="required">
                </div>
                <div class="icon_form button">
                    <input id="boton" type="submit" class="btn fondo2 cblanco cblancoh fondoh1" name="resgistrar" value="Registrar">
                </div>
            </form>
            <span class="forget"><a id="link1" class="color1 colorh2" href="../index.php">Pagina de inicio</a></span>
            <span class="forget"><a id="link2" class="color1 colorh2" href="login.php">Ya poseo una cuenta</a></span>
        </div>

        <!-- fin section 1 -->
        <script src="../js/sweetalert.min.js">
            document.querySelector('ul.examples li.warning.confirm button').onclick = function(){
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this imaginary file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, delete it!',
                closeOnConfirm: false
            },
            function(){
                swal("Deleted!", "Your imaginary file has been deleted!", "success");
            });
        };
        </script>
    </section>
    <?php include("../layout/footer.php")?>
    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.prettyPhoto.js"></script>
    <script src="../js/jquery.isotope.min.js"></script>
    <script src="../js/wow.min.js"></script>
    <script src="../js/main.js"></script>

    <script src="../js/bootstrap.min.js" type="text/javascript"></script>
</body>

</html>

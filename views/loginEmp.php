<!DOCTYPE html>
<?php session_start();?>
<html class="col-xs-12.fluid">
    <head>
        <title>Ingreso - ProfileUp</title>
         <!-- Bootstrap -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/font-awesome.min.css">
        <link href="../css/animate.min.css" rel="stylesheet">
        <link href="../css/prettyPhoto.css" rel="stylesheet">     

        <link href="../css/main1.css" rel="stylesheet">
        <link href="../css/responsive.css" rel="stylesheet">
        
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../css/login.css" media="screen">
        <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">


        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
    </head>
    <body class="fondo1 col-xs-12-fluid">

        <section id="principal" class="col-xs-12-fluid">

            <!-- section 1 -->

            <div class="contenedor border2">
                <figure class="logo">
                    <a href="../index.php"><img src="../fonts/loGo_3.svg" alt="Smart Up Logo" class="../fonts/loGo_3.svg"></a>
                </figure>

                <p>Sistema de gestión de hojas de vida y pruebas psicotecnicas.<br>Ingresa con tu usuario y contraseña</p>

                <form class="login_form" action="../controller/controllerUsuarioEmp.php" method="POST">
                    <div class="icon_form usuario">
                        <label class="fondo1 cblanco usuario" for="usuario" title="Usuario"></label>
                        <input id="usuario" type="text" name="documento" class="input" placeholder="Usuario" required="required">
                    </div>
                    <div class="icon_form contraseña">
                        <label class="fondo1 cblanco contrasena" for="contraseña" title="Contraseña"></label>
                        <input id="contraseña" type="password" autocomplete="off" name="contrasena" class="input" placeholder="Contraseña" required="required">
                    </div>
                    <div class="icon_form button">
                        <input type="submit" class="btn fondo2 cblanco cblancoh fondoh1" name ="ingresar" value="Ingresar">
                    </div>

                    <span class="forget"><a class="color1 colorh2" href="recuperarPass.php">¿Olvidó su contraseña?</a></span>
                    <span class="forget"><a class="color1 colorh2" href="register.php">¿Aún no esta registrado?</a></span>
                    <span class="forget"><a class="color1 colorh2" href="../index.php">pagina de inicio</a></span>
                </form>

            </div>

            <!-- fin section 1 -->

        </section>
        <?php include("../layout/footer.php");?>
        <!--  jQuery Bootstraps JavaScript plugins -->
        <script src="../js/jquery.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/jquery.prettyPhoto.js"></script>
        <script src="../js/jquery.isotope.min.js"></script>   
        <script src="../js/wow.min.js"></script>
        <script src="../js/main.js"></script>
        
        <script src="../js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>